import config
import telebot
import sqlite3


class Database:
    def __init__(self, database):               # конструктор объекта бд (открытие), из config
        self.connection = sqlite3.connect(database)
        self.cursor = self.connection.cursor()

    def close(self):                            # закрытие бд
        self.connection.close()


bot = telebot.TeleBot(config.token)             # токен бота из config


def check_status(user_id):
    db_worker = Database(config.database_name)
    db_worker.cursor.execute("SELECT * FROM interview WHERE user_id = ?;", [user_id])
    step = db_worker.cursor.fetchone()
    db_worker.close()
    if step is not None:
        return step[1]
    else:
        return -1
# знакомимся без проверки на валидность


@bot.message_handler(commands=["start"])  # при команде /start спрашиваем фио, step = 0 - знакомство
def init_start(message):
    db_worker = Database(config.database_name)
    db_worker.cursor.execute("SELECT * FROM interview WHERE user_id = ?;", [message.from_user.id])
    user_add = db_worker.cursor.fetchone()
    if user_add is None:
        user_add = (message.from_user.id, 0, '', '', '')
        db_worker.cursor.execute("INSERT INTO interview VALUES(?, ?, ?, ?, ?);", user_add)
        bot.send_message(message.chat.id, 'Ваше Фамилия Имя Отчество(при наличии, если нет "-") (вводить через пробел)')
        db_worker.connection.commit()
        db_worker.close()
    else:
        bot.send_message(message.chat.id, 'Ваша анкета уже была записана, для сброса введите: /reset')


@bot.message_handler(func=lambda message: check_status(message.from_user.id) == 0)  # обработка фио, если корректно - добавляем в бд
def init_fio(message):
    fio = tuple(str(message.text).split())
    if len(fio) == 3:
        db_worker = Database(config.database_name)
        upd_column = (4, message.from_user.id)          # обновляем step = 4 -> фио добавлено
        upd_column = fio+upd_column  # каждый раз перезаписывать ид и шаг, надо бы переделать
        db_worker.cursor.execute("UPDATE interview SET name = ?, surname = ?, surname_2 = ?, status = ? WHERE user_id = ?", upd_column)
        db_worker.connection.commit()
        db_worker.close()
    else:
        bot.send_message(message.chat.id, 'Ошибка ввода')


@bot.message_handler(commands=["reset"])  # при команде /reset сбрасываем анкету, удаляем запись из бд
def reset_start(message):
    bot.send_message(message.chat.id, 'Анкета была успешно сброшена. Для создания новой - /start')
    db_worker = Database(config.database_name)
    db_worker.cursor.execute("DELETE FROM interview WHERE user_id = ?", [message.from_user.id])
    db_worker.connection.commit()
    db_worker.close()


@bot.message_handler(content_types=["text"]) # для проверки изменений, чтоб в бд не лазить, вывод в виде кортежа (пока и так сойдет)
def check_form(message):                      # пока для теста используется регистрация одного человека - себя, и для проверки по умолчанию свой ид аккаунта
    if message.text == "test":
        db_worker = Database(config.database_name)
        db_worker.cursor.execute("SELECT * FROM interview WHERE user_id = ?;", [message.from_user.id])
        user_form = db_worker.cursor.fetchone()
        db_worker.close()
        if user_form is not None:
            bot.send_message(message.chat.id, 'Из бд вытянуто: ' + str(user_form))
        else:
            bot.send_message(message.chat.id, 'Записей с таким id нет')

# вечная мейн функция


if __name__ == '__main__':
     bot.infinity_polling()
